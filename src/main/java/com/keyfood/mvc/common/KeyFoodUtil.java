package com.keyfood.mvc.common;

import com.keyfood.mvc.domain.Item;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Garis M. Suero
 */
public class KeyFoodUtil {

    public final static String COMMON_DATE_FORMAT = "mm/dd/yyyy";
    
    public static Date getDateOrNull(String value, String format) {
        if (value == null) 
            return null;

        try {
            SimpleDateFormat dt = new SimpleDateFormat(format);
            return dt.parse(value.trim());
        }
        catch (ParseException ex) {
            return null;
        }
    }
    public static int getItemIndexInList(List<Item> items, Item item) {
        Iterator<Item> itemIterator = items.iterator();
        int index = 0;
        while (itemIterator.hasNext()) {
            Item itemIt = itemIterator.next();
            if (item.equals(itemIt))
                return index;
            
            index++;
        }
        return -1;
    }
}
