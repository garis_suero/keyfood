package com.keyfood.mvc.controller;

import com.keyfood.mvc.common.KeyFoodUtil;
import com.keyfood.mvc.domain.Detail;
import com.keyfood.mvc.domain.Item;
import com.keyfood.mvc.service.ItemService;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author Garis M. Suero
 */
@ManagedBean(name = "BuyerDealBean")
@ViewScoped
public class BuyerDealBean implements Serializable {

    private static final Logger logger = Logger.getLogger(BuyerDealBean.class);
    @ManagedProperty(value = "#{itemService}")
    private ItemService itemService;

    /**
     * Creates a new instance of BuyerDealBean
     */
    public BuyerDealBean() {
    }

    @PostConstruct
    public void init() {

        if (logger.isDebugEnabled()) {
            logger.debug("init() exiting with ");
        }
    }

    public void handleFileUpload(FileUploadEvent event) {

        try {
            Workbook workbook = WorkbookFactory.create(event.getFile().getInputstream()); // 
            XSSFSheet sheet = (XSSFSheet) workbook.getSheetAt(0);
            Iterator rowIterator = sheet.rowIterator();
            boolean saveRow = false;
            boolean validRow = false;
            List<Item> items = new ArrayList<Item>();
            while (rowIterator.hasNext()) {
                XSSFRow hssfRow = (XSSFRow) rowIterator.next();
                Iterator iterator = hssfRow.cellIterator();
                List cellTempList = new ArrayList();

                boolean firstCell = true;
                Item item = new Item();
                Detail detail = new Detail();
                while (iterator.hasNext()) {
                    XSSFCell cell = (XSSFCell) iterator.next();

                    if (!saveRow && cell.getColumnIndex() == 0 && "Item #".equalsIgnoreCase(cell.toString())) {
                        saveRow = true;
                        validRow = true;
                        break;
                    } else if (firstCell && cell.getColumnIndex() > 0) {
                        validRow = false;
                        continue;
                    } else if (saveRow) {
                        try {
                        switch (cell.getColumnIndex()) {
                            case 0:
                                item.setItemid(new BigDecimal(cell.toString()));
                                break;
                            case 1:
                                item.setDesItem(cell.toString().trim());
                                break;
                            case 8:
                                System.out.println(cell.toString() + "Prueba");
                                
                                detail.setPack(((int) Double.valueOf(cell.toString()).doubleValue()));
                                break;
                            case 9:
                                detail.setItemSize(Double.valueOf(cell.toString()));
                                break;
                            case 10:
                                detail.setListCost(((int) Double.valueOf(cell.toString()).doubleValue()));
                                break;
                            case 11:
                                detail.setSrUnits(((int) Double.valueOf(cell.toString()).doubleValue()));
                                break;
                            case 12:
                                detail.setSrPrice(Double.valueOf(cell.toString()));
                                break;
                            case 13:
                                detail.setSrBrand(((int) Double.valueOf(cell.toString()).doubleValue()));
                                break;
                            case 14:
                                detail.setDiAmount(Double.valueOf(cell.toString()));
                                break;
                            case 15:
                                detail.setDiSdate(KeyFoodUtil.getDateOrNull(cell.toString(), KeyFoodUtil.COMMON_DATE_FORMAT));
                                break;
                            case 16:
                                detail.setDiEdate(KeyFoodUtil.getDateOrNull(cell.toString(), KeyFoodUtil.COMMON_DATE_FORMAT));
                                break;
                            case 17:
                                detail.setOiAmount(Double.valueOf(cell.toString()));
                                break;
                            case 18:
                                detail.setOiSdate(KeyFoodUtil.getDateOrNull(cell.toString(), KeyFoodUtil.COMMON_DATE_FORMAT));
                                break;
                            case 19:
                                detail.setOiEdate(KeyFoodUtil.getDateOrNull(cell.toString(), KeyFoodUtil.COMMON_DATE_FORMAT));
                                break;
                            case 20:
                                detail.setRevalue(Double.valueOf(cell.toString()));
                                break;
                            case 21:
                                detail.setRwUnits(((int) Double.valueOf(cell.toString()).doubleValue()));
                                break;
                            case 22:
                                detail.setRwPrice(Double.valueOf(cell.toString()));
                                break;
                            case 23:
                                detail.setClimit(((int) Double.valueOf(cell.toString()).doubleValue()));
                                break;
                            case 24:
                                detail.setMinPurchase(Double.valueOf(cell.toString()));
                                break;
                            case 25:
                                detail.setCtype(((int) Double.valueOf(cell.toString()).doubleValue()));
                                break;
                            case 26:
                                detail.setAddress1(cell.toString());
                                break;
                            case 27:
                                detail.setAddress2(cell.toString());
                                break;
                            case 28:
                                detail.setCity(cell.toString());
                                break;
                            case 29:
                                detail.setState(cell.toString());
                                break;
                            case 30:
                                detail.setZipcode(cell.toString());
                                break;
                        }
                        } catch (NumberFormatException ex) {
                            //no problem....
                        }
                        cellTempList.add(cell);
                    }
                    firstCell = false;
                }
                if (saveRow && validRow) {
                    if (!items.contains(item)) {
                        item.addDetail(detail);
                        items.add(item);
                    } else {
                        Item itemFound = items.get(KeyFoodUtil.getItemIndexInList(items, item));
                        itemFound.addDetail(detail);
                    }
                }
            }
            items.remove(0);
            itemService.create(items);
        }
        catch (InvalidFormatException ex) {
            java.util.logging.Logger.getLogger(BuyerDealBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (IOException ex) {
            java.util.logging.Logger.getLogger(BuyerDealBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        FacesMessage msg = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded." + org.apache.poi.Version.getVersion());
        FacesContext.getCurrentInstance().addMessage(null, msg);
        if (logger.isDebugEnabled()) {
            logger.debug("Succesful" + event.getFile().getFileName() + " is uploaded.");
            logger.debug(" size: " + event.getFile().getSize());
        }

    }

    public ItemService getItemService() {
        return itemService;
    }

    public void setItemService(ItemService itemService) {
        this.itemService = itemService;
    }
}
