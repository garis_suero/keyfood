package com.keyfood.mvc.controller;

import com.keyfood.mvc.domain.Detail;
import com.keyfood.mvc.domain.Item;
import com.keyfood.mvc.service.ItemService;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Garis M. Suero
 */
@ManagedBean(name = "ViewDataBean")
@ViewScoped
public class ViewDataBean implements Serializable {

    private static final Logger logger = Logger.getLogger(ViewDataBean.class);
    @ManagedProperty(value = "#{itemService}")
    private ItemService itemService;

    private List<Item> items;
    private List<Detail> details;
    private Item selectedItem;
    /**
     * Creates a new instance of BuyerDealBean
     */
    public ViewDataBean() {
    }

    @PostConstruct
    public void init() {
        setItems(itemService.getAllItems());
        if (logger.isDebugEnabled()) {
            logger.debug("init() ");
        }
    }
    
    public void onRowSelect(SelectEvent event) {  
        selectedItem = (Item) event.getObject();
        details = (List<Detail>) selectedItem.getDetailCollection();
        FacesMessage msg = new FacesMessage("Item Selected", selectedItem.getDesItem());  
  
        FacesContext.getCurrentInstance().addMessage(null, msg);  
    }  

    public void setItemToUse() {
        
    }
    

    public ItemService getItemService() {
        return itemService;
    }

    public void setItemService(ItemService itemService) {
        this.itemService = itemService;
    }

    public List<Detail> getDetails() {
        return details;
    }

    public void setDetails(List<Detail> details) {
        this.details = details;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public Item getSelectedItem() {
        return selectedItem;
    }

    public void setSelectedItem(Item selectedItem) {
        this.selectedItem = selectedItem;
    }
    
    
}
