/*
 *
 */
package com.keyfood.mvc.dao;

import com.keyfood.mvc.domain.BaseDTO;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Garis M. Suero
 * @version 1.0
 */
public abstract class AbstractDao < K, T extends BaseDTO >
        implements BaseDao <K, T > {
    
    /**
     * 
     */
    private final static Logger logger = Logger.getLogger(AbstractDao.class);


    /**
     * <p>The {@link EntityManager} instance</p>.
     */
    @PersistenceContext
    protected EntityManager entityManager;

    @Transactional(propagation= Propagation.REQUIRED)
    @Override
    public T create(T entity) {
        try {
            entityManager.persist(entity);
            entityManager.refresh(entity);
        } catch (Throwable t) {
            StringBuilder builder = new StringBuilder("Error creating entity");
            builder.append(entity.toString());
            builder.append(" - ");
            builder.append(t.getMessage());

            logger.error(builder.toString());
        }

        return entity;
    }

    @Transactional(propagation= Propagation.REQUIRED)
    @Override
    public boolean delete(T entity) {
        boolean result = true;

        try {
            entityManager.remove(entity);
        } catch(Throwable t) {
            StringBuilder builder = new StringBuilder("Error removing entity");
            builder.append(entity.toString());
            builder.append(" - ");
            builder.append(t.getMessage());
            
            logger.error(builder.toString());
            result = false;
        }

        return result;
    }

    @Transactional(propagation= Propagation.REQUIRED)
    @Override
    public T update(T entity) {
        T dto = (T)new BaseDTO();

        try {
            dto = entityManager.merge(entity);
        } catch (Throwable t) {
            StringBuilder builder = new StringBuilder("Error creating entity");
            builder.append(entity.toString());
            builder.append(" - ");
            builder.append(t.getMessage());

            logger.error(builder.toString());
        }

        return dto;
    }



    /**
     * <p>Get the entity manager</p>.
     * @return the {@link EntityManager{
     */
    public EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * <p>Set the entity manager</p>.
     * @param entityManager the {@link EntityManager} to set
     */
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
}
