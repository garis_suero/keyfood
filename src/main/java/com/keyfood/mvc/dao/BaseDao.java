/*
 *
 */
package com.keyfood.mvc.dao;

import com.keyfood.mvc.domain.BaseDTO;
import java.util.Collection;
import java.util.Map;


/**
 * 
 * @author Garis M. Suero
 * @version 1.0
 * @param <K> The key of the element
 * @param <T> The element
 */
public interface BaseDao < K, T extends BaseDTO > {
    /**
     * <p>Find the element represented by the given key</p>.
     * @param key the key identifying the element.
     * @return the element instance if found.
     */
    T find(K key);

    /**
     * <p>Find the element represented by an optional complex set of keys</p>.
     * @param key the additional keys identifying the element.
     * @param keys additional keys identifying the element.
     * @return a collection of the elements found.
     */
    Collection < T > find(K key, Map <String, Object> keys);

    /**
     * <p>Find the element represented by a set of keys or parameters</p>.
     * @param keys the key parameters.
     * @return a collection of the elements found.
     */
    Collection < T > find(Object... keys);

    /**
     * <p>Persist the entity into the database</p>.
     * @param entity the entity to persist.
     * @return the persisted entity.
     */
    T create(T entity);

    /**
     * <p>Merge the entity with the database</p>.
     * @param entity the existing entity to merge.
     * @return the merged entity.
     */
    T update(T entity);

    /**
     * <p>Delete the entity instance from the database</p>.
     * @param entity the existing entity to delete.
     * @return true if successful.
     */
    boolean delete(T entity);
}
