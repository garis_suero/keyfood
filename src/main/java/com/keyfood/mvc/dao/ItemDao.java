package com.keyfood.mvc.dao;

import com.keyfood.mvc.domain.Detail;
import com.keyfood.mvc.domain.Item;
import java.math.BigDecimal;
import java.util.List;
/**
 *
 * @author Cuantico
 */
public interface ItemDao {

    public Item create(Item item);
    public Item update(Item item);
    public void delete(Item item);
    public Item getItemById(BigDecimal itemId);
    public Detail create(Detail detail);
    public Detail update(Detail detail);
    public void delete(Detail detail);
    public List<Item> getAllItems();
}
