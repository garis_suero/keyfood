package com.keyfood.mvc.dao;


import com.keyfood.mvc.domain.Detail;
import com.keyfood.mvc.domain.Item;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Cuantico
 */
@Repository("itemDao")
public class ItemDaoImpl implements ItemDao {

    @PersistenceContext
    private EntityManager em;
    
    @Override
    public Item create(Item item) {
        em.persist(item);
        return item;
    }

    @Override
    public Item update(Item item) {
        return em.merge(item);
    }

    @Override
    public void delete(Item item) {
        item = em.merge(item);
        em.remove(item);
    }
    
    @Override
    public Detail create(Detail detail) {
        em.persist(detail);
        return detail;
    }

    @Override
    public Detail update(Detail detail) {
        return em.merge(detail);
    }

    @Override
    public void delete(Detail detail) {
        detail = em.merge(detail);
        em.remove(detail);
    }
    @Override
    public Item getItemById(BigDecimal itemId) {
        Query query = em.createNamedQuery("Item.findByItemid");
        query.setParameter("itemid", itemId);
        try {
            Item item = (Item) query.getSingleResult();
            return item;
        } catch (NoResultException ex) {
            return null;
        }
    }
    
    @Override
    public List<Item> getAllItems() {
        Query query = em.createNamedQuery("Item.findAll");
        List<Item> ret = query.getResultList();
        return ret;
    }
}
