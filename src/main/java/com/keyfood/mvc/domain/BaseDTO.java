/*
 *
 */
package com.keyfood.mvc.domain;

import java.io.Serializable;

/**
 *
 * @author Garis M. Suero
 * @version 1.0
 */
public class BaseDTO implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1891626253L;


    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        builder.append(this.getClass().getName());
        builder.append("]");
        
        return builder.toString();
    }
}
