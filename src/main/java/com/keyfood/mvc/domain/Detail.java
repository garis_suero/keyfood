package com.keyfood.mvc.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Garis M. Suero
 */
@Entity
@Table(name = "DETAIL")
@NamedQueries({
    @NamedQuery(name = "Detail.findAll", query = "SELECT d FROM Detail d"),
    @NamedQuery(name = "Detail.findByPack", query = "SELECT d FROM Detail d WHERE d.pack = :pack"),
    @NamedQuery(name = "Detail.findByListCost", query = "SELECT d FROM Detail d WHERE d.listCost = :listCost"),
    @NamedQuery(name = "Detail.findBySrUnits", query = "SELECT d FROM Detail d WHERE d.srUnits = :srUnits"),
    @NamedQuery(name = "Detail.findBySrPrice", query = "SELECT d FROM Detail d WHERE d.srPrice = :srPrice"),
    @NamedQuery(name = "Detail.findByDetailid", query = "SELECT d FROM Detail d WHERE d.detailid = :detailid")})
public class Detail implements Serializable {
    private static final long serialVersionUID = 1L;
    @Column(name = "PACK")
    private int pack;
    @Column(name = "ITEM_SIZE")
    private Double itemSize;
    @Column(name = "LIST_COST")
    private int listCost;
    @Column(name = "SR_UNITS")
    private int srUnits;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "SR_PRICE")
    private Double srPrice;
    @Column(name = "SR_BRAND")
    private int srBrand;
    @Column(name = "DI_AMOUNT")
    private Double diAmount;
    @Column(name = "DI_SDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date diSdate;
    @Column(name = "DI_EDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date diEdate;
    @Column(name = "OI_AMOUNT")
    private Double oiAmount;
    @Column(name = "OI_SDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date oiSdate;
    @Column(name = "OI_EDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date oiEdate;
    @Column(name = "REVALUE")
    private Double revalue;
    @Column(name = "RW_UNITS")
    private int rwUnits;
    @Column(name = "RW_PRICE")
    private Double rwPrice;
    @Column(name = "CLIMIT")
    private int climit;
    @Column(name = "MIN_PURCHASE")
    private Double minPurchase;
    @Column(name = "CTYPE")
    private int ctype;
    @Size(max = 80)
    @Column(name = "ADDRESS1")
    private String address1;
    @Size(max = 80)
    @Column(name = "ADDRESS2")
    private String address2;
    @Size(max = 80)
    @Column(name = "CITY")
    private String city;
    @Size(max = 80)
    @Column(name = "STATE")
    private String state;
    @Size(max = 5)
    @Column(name = "ZIPCODE")
    private String zipcode;
    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="DETAILSEQUENCE")
    @SequenceGenerator(name="DETAILSEQUENCE", sequenceName="DETAILSEQUENCE")
    @Column(name = "DETAILID")
    private BigDecimal detailid;
    
    @JoinColumn(name = "ITEMID", referencedColumnName = "ITEMID")
    @ManyToOne
    private Item itemid;

    public Detail() {
    }

    public Detail(BigDecimal detailid) {
        this.detailid = detailid;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getClimit() {
        return climit;
    }

    public void setClimit(int climit) {
        this.climit = climit;
    }

    public int getCtype() {
        return ctype;
    }

    public void setCtype(int ctype) {
        this.ctype = ctype;
    }

    public BigDecimal getDetailid() {
        return detailid;
    }

    public void setDetailid(BigDecimal detailid) {
        this.detailid = detailid;
    }

    public Double getDiAmount() {
        return diAmount;
    }

    public void setDiAmount(Double diAmount) {
        this.diAmount = diAmount;
    }

    public Date getDiEdate() {
        return diEdate;
    }

    public void setDiEdate(Date diEdate) {
        this.diEdate = diEdate;
    }

    public Date getDiSdate() {
        return diSdate;
    }

    public void setDiSdate(Date diSdate) {
        this.diSdate = diSdate;
    }

    public Double getItemSize() {
        return itemSize;
    }

    public void setItemSize(Double itemSize) {
        this.itemSize = itemSize;
    }

    public Item getItemid() {
        return itemid;
    }

    public void setItemid(Item itemid) {
        this.itemid = itemid;
    }

    public int getListCost() {
        return listCost;
    }

    public void setListCost(int listCost) {
        this.listCost = listCost;
    }

    public Double getMinPurchase() {
        return minPurchase;
    }

    public void setMinPurchase(Double minPurchase) {
        this.minPurchase = minPurchase;
    }

    public Double getOiAmount() {
        return oiAmount;
    }

    public void setOiAmount(Double oiAmount) {
        this.oiAmount = oiAmount;
    }

    public Date getOiEdate() {
        return oiEdate;
    }

    public void setOiEdate(Date oiEdate) {
        this.oiEdate = oiEdate;
    }

    public Date getOiSdate() {
        return oiSdate;
    }

    public void setOiSdate(Date oiSdate) {
        this.oiSdate = oiSdate;
    }

    public int getPack() {
        return pack;
    }

    public void setPack(int pack) {
        this.pack = pack;
    }

    public Double getRevalue() {
        return revalue;
    }

    public void setRevalue(Double revalue) {
        this.revalue = revalue;
    }

    public Double getRwPrice() {
        return rwPrice;
    }

    public void setRwPrice(Double rwPrice) {
        this.rwPrice = rwPrice;
    }

    public int getRwUnits() {
        return rwUnits;
    }

    public void setRwUnits(int rwUnits) {
        this.rwUnits = rwUnits;
    }

    public int getSrBrand() {
        return srBrand;
    }

    public void setSrBrand(int srBrand) {
        this.srBrand = srBrand;
    }

    public Double getSrPrice() {
        return srPrice;
    }

    public void setSrPrice(Double srPrice) {
        this.srPrice = srPrice;
    }

    public int getSrUnits() {
        return srUnits;
    }

    public void setSrUnits(int srUnits) {
        this.srUnits = srUnits;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += ( detailid != null ? detailid.hashCode() : 0 );
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!( object instanceof Detail )) {
            return false;
        }
        Detail other = (Detail) object;
        if (( this.detailid == null && other.detailid != null ) || ( this.detailid != null && !this.detailid.equals(other.detailid) )) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.keyfood.mvc.domain.Detail[ detailid=" + detailid + " ]";
    }

}
