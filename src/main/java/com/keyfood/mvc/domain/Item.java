package com.keyfood.mvc.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Garis M. Suero
 */
@Entity
@Table(name = "ITEM")
@NamedQueries({
    @NamedQuery(name = "Item.findAll", query = "SELECT i FROM Item i"),
    @NamedQuery(name = "Item.findByItemid", query = "SELECT i FROM Item i WHERE i.itemid = :itemid"),
    @NamedQuery(name = "Item.findByDesItem", query = "SELECT i FROM Item i WHERE i.desItem = :desItem")})
public class Item implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ITEMID")
    private BigDecimal itemid;
    @Size(max = 60)
    @Column(name = "DES_ITEM")
    private String desItem;
    @OneToMany(mappedBy = "itemid", cascade= CascadeType.ALL, fetch= FetchType.EAGER)
    private Collection<Detail> detailCollection;

    public Item() {
    }

    public Item(BigDecimal itemid) {
        this.itemid = itemid;
    }

    public BigDecimal getItemid() {
        return itemid;
    }

    public void setItemid(BigDecimal itemid) {
        this.itemid = itemid;
    }

    public String getDesItem() {
        return desItem;
    }

    public void setDesItem(String desItem) {
        this.desItem = desItem;
    }

    public Collection<Detail> getDetailCollection() {
        return detailCollection;
    }

    public void setDetailCollection(Collection<Detail> detailCollection) {
        this.detailCollection = detailCollection;
    }

    public void addDetail(Detail detail) {
        if (getDetailCollection() == null){
            detailCollection = new ArrayList<Detail>();
            detailCollection.add(detail);
        } else if (!getDetailCollection().contains(detail)) {
            this.getDetailCollection().add(detail);
        }
    }
    @Override
    public int hashCode() {
        int hash = 0;
        hash += ( itemid != null ? itemid.hashCode() : 0 );
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!( object instanceof Item )) {
            return false;
        }
        Item other = (Item) object;
        if (( this.itemid == null && other.itemid != null ) || ( this.itemid != null && !this.itemid.equals(other.itemid) )) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Item{" + "itemid=" + itemid + ", desItem=" + desItem + '}';
    }



}
