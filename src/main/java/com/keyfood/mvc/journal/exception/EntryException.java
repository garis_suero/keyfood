/*
 * 
 */
package com.keyfood.mvc.journal.exception;

import java.io.Serializable;

/**
 *
 * @author Garis M. Suero
 */
public class EntryException extends Exception
    implements Serializable {
    
    /**
     * 
     */
    public EntryException() {
    }

    /**
     * 
     * @param message 
     */
    public EntryException(String message) {
        super(message);
    }

    /**
     * 
     * @param cause 
     */
    public EntryException(Throwable cause) {
        super(cause);
    }

    /**
     * 
     * @param message
     * @param cause 
     */
    public EntryException(String message, Throwable cause) {
        super(message, cause);
    }

}
