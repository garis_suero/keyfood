/*
 * 
 */
package com.keyfood.mvc.journal.exception;

import java.io.Serializable;

/**
 *
 * @author Garis M. Suero
 */
public class NotValidEntryException extends Exception
    implements Serializable {
    
    /**
     * 
     */
    public NotValidEntryException() {
    }

    /**
     * 
     * @param message 
     */
    public NotValidEntryException(String message) {
        super(message);
    }

    /**
     * 
     * @param cause 
     */
    public NotValidEntryException(Throwable cause) {
        super(cause);
    }

    /**
     * 
     * @param message
     * @param cause 
     */
    public NotValidEntryException(String message, Throwable cause) {
        super(message, cause);
    }

}
