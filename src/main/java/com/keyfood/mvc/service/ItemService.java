package com.keyfood.mvc.service;

import com.keyfood.mvc.domain.Detail;
import com.keyfood.mvc.domain.Item;
import java.util.List;
/**
 *
 * @author Garis M. Suero
 */
public interface ItemService {
            
    public List<Item> getAllItems();
    public boolean create(List<Item> items);
    public Item  create(Item item);
    public Item  update(Item item);
    public boolean delete(Item item);
    public Detail create(Detail detail);
    public Detail update(Detail detail);
    public boolean delete(Detail detail);
}
