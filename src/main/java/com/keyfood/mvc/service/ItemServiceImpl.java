package com.keyfood.mvc.service;

import com.keyfood.mvc.dao.ItemDao;
import com.keyfood.mvc.domain.Detail;
import com.keyfood.mvc.domain.Item;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Garis M. Suero
 */
@Service
public class ItemServiceImpl implements ItemService {

    @Autowired
    private ItemDao itemDao;
    private static final Logger logger = Logger.getLogger(ItemServiceImpl.class);

    @Override
    public List<Item> getAllItems() {
        return itemDao.getAllItems();
    }
    @Override
    public boolean create(List<Item> items) {
        try {
            Iterator<Item> itemIterator = items.iterator();
            while (itemIterator.hasNext()) {
                Item item = itemIterator.next();
                Item itemCreated = itemDao.getItemById(item.getItemid());
                Collection<Detail> details = item.getDetailCollection();
                if (itemCreated == null) {
                    item = create(item);
                } else {
                    item = itemCreated;
                } 
                Iterator<Detail> detailIter = details.iterator();
                while (detailIter.hasNext()) {
                    Detail detail = detailIter.next();
                    detail.setItemid(item);
                    create(detail);
                }
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    public Item create(BigDecimal itemId, String itemDescription) {
        Item item = new Item();
        item.setItemid(itemId);
        item.setDesItem(itemDescription);
        return itemDao.create(item);
    }

    @Override
    public Item create(Item item) {
        return itemDao.create(item);
    }

    @Override
    public Item update(Item item) {
        return itemDao.update(item);
    }

    @Override
    public boolean delete(Item item) {
        itemDao.delete(item);
        return true;
    }
    
    @Override
    public Detail create(Detail detail) {
        return itemDao.create(detail);
    }

    @Override
    public Detail update(Detail detail) {
        return itemDao.update(detail);
    }

    @Override
    public boolean delete(Detail detail) {
        itemDao.delete(detail);
        return true;
    }

    public void setItemDao(ItemDao itemDao) {
        this.itemDao = itemDao;
    }

    public ItemDao getItemDao() {
        return itemDao;
    }
    
}
